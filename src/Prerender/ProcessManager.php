<?php
namespace Nullsju\Prerender;

use Symfony\Component\Process\Process;

/**
 * This ProcessManager is a simple wrapper to enable parallel processing using Symfony Process component.
 */
class ProcessManager
{
    private $logger;

    private $queue;

    private $processQueue = [];

    private $currentProcesses = [];

    private $rootDir;

    private $baseCommand;

    private $poll;

    private $limit;

    public function __construct(Logger $logger, Queue $queue, $rootDir, $poll, $limit)
    {
        $this->logger = $logger->get('Process');
        $this->queue = $queue;
        $this->rootDir = $rootDir;
        $this->poll = $poll;
        $this->limit = $limit;
        $this->baseCommand = sprintf("/%s/bin/prerender prerender:process", trim($this->rootDir, '/'));
        $this->logger->info("ProcessManager initialized");
    }

    public function run()
    {
        $this->logger->info("ProcessManager started");
        $this->resume();
    }

    protected function resume()
    {
        $this->logger->info("Resuming");
        $halted = $this->queue->getHalted();

        if (count($halted) > 0) {
            $this->populate($halted);
            $this->process();
        } else {
            $this->listen();
        }
    }

    protected function populate($queued)
    {
        $this->logger->info(sprintf("Adding %s process to queue", count($queued)));
        foreach ($queued as $item) {
            $command = sprintf("%s --id=%s", $this->baseCommand, $item['id']);

            $this->queue->enqueue($item['id']);
            $this->processQueue[] = new Process($command);
        }
    }

    protected function listen()
    {
        while (count($this->currentProcesses) > 0) {
            $this->logger->info("Waiting for all processes to finish");
            $this->cleanQueue();
            sleep(5);
        }

        $this->logger->info("Listening for work to do");

        while (count($this->processQueue) === 0) {

            $this->cleanQueue();
            $queued = $this->queue->getQueued();

            if (count($queued) > 0) {
                $this->logger->info("Found entries in queue, populating");
                $this->populate($queued);
            } else {
                $this->logger->info("Queue is empty, waiting...");
                sleep(5);
            }
        }

        $this->process();
    }

    protected function process()
    {
        $this->logger->info("Processing queue");
        while (count($this->processQueue) > 0) {

            usleep($this->poll);

            $this->cleanQueue();

            if (count($this->currentProcesses) < $this->limit) {
                $process = array_shift($this->processQueue);
                $pid = $this->execute($process);
                if ($pid) {
                    $this->currentProcesses[$pid] = $process;
                }
            } else {
                $this->logger->info("To many processes running, waiting...");
                sleep(5);
            }
        }

        $this->logger->info("Queue is finished");
        $this->listen();
    }

    protected function execute($process)
    {
        if ($this->validateProcess($process)) {
            $process->start();
            $this->logger->info(sprintf("Starting process with pid %s", $process->getPid()));
            return $process->getPid();
        }

        return null;
    }

    protected function cleanQueue()
    {
        foreach ($this->currentProcesses as $key => $process) {
            if (!$process->isRunning()) {
                if ($process->getExitCode() === 0) {
                    $this->logger->info(sprintf("Process with pid %s has finshed without errors", $key));
                    unset($this->currentProcesses[$key]);
                } elseif ($process->getExitCode() === 2) {
                    $this->logger->error("Prerender limit exceeded for this hour");
                    $this->halt();
                } elseif ($process->getExitCode() === 3) {
                    $this->logger->error("Something went wrong with prerendering of url, test this command: " . $process->getCommandline());
                    unset($this->currentProcesses[$key]);
                } else {
                    $this->logger->error("Something went very wrong, unexpected exitcode from worker. Debug it: " . $process->getCommandline());
                    unset($this->currentProcesses[$key]);
                }
            }
        }
    }

    protected function halt()
    {
        $this->logger->error("Halting");
        exit(0);
    }

    /**
     * @param Process
     */
    protected function validateProcess($process)
    {
        return $process instanceof Process;
    }
}
