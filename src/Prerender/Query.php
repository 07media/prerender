<?php

namespace Nullsju\Prerender;

class Query
{
    /**
     * The table this query works on
     */
    public $table = null;

    /**
     * The order the entries will be returned
     */
    public $order;

    /**
     * The limit of entries this query must return
     */
    public $limit = 0;

    /**
     * The offset this query must skip
     */
    public $offset = 0;

    /**
     * The id of the entry
     */
    public $id = 0;

    /**
     * Entries must satisfy this filter
     */
    public $where = null;

    /**
     * The fields to be selected
     */
    public $select = null;

    /**
     * Every time a method which returns data is called, the query must be set up all over again.
     */
    private $executed = false;

    /**
     * Query constructor
     *
     * @param string $name The name of the table this query works on
     */
    public function __construct($name)
    {
        $this->table = $name;
        $this->order = array('key' => 'id', 'mode' => 'ASC');
    }

    /**
     * Mark this query as executed
     */
    public function run()
    {
        $this->executed = true;
    }

    /**
     * Whether this query was run or not
     */
    public function was_run()
    {
        return $this->executed;
    }
}
