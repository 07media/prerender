<?php

namespace Nullsju\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\Table;

class DisplayQueueCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('prerender:display:queue')
            ->setDescription('Add a entry to the queue')
            ->addOption('status', 's', InputOption::VALUE_OPTIONAL, 'Status')
            ->addOption('limit', 'l', InputOption::VALUE_OPTIONAL, 'Limit')
            ->addOption('offset', 'o', InputOption::VALUE_OPTIONAL, 'Offset');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $prerender = new \Nullsju\Prerender();
        $database = $prerender->container->get('database');

        $query = $database->table('queue');

        if ($status = $input->getOption('status')) {
            $query->where(['status' => $status]);
        }
        if ($offset = $input->getOption('offset')) {
            $query->offset($offset);
        }
        if ($limit = $input->getOption('limit')) {
            $query->offset($limit);
        }

        $lines = [];
        $colors = ['blue', 'yellow', 'green', 'red'];
        $rows = $query->all();

        foreach ($rows as $key => $row) {

            $line = [];
            $color = isset($colors[$row['status']]) ? $colors[$row['status']] : 'white';

            foreach ($row as $key => $column) {
                $line[] = "<fg={$color}>{$column}</fg={$color}>";
            }

            $lines[] = $line;
        }

        $table = new Table($output);


        $table
            ->setHeaders(
                array(
                    '<fg=white;options=bold>URL</fg=white;options=bold>',
                    '<fg=white;options=bold>STATUS</fg=white;options=bold>',
                    '<fg=white;options=bold>QUEUED</fg=white;options=bold>',
                    '<fg=white;options=bold>STARTED</fg=white;options=bold>',
                    '<fg=white;options=bold>FINISHED</fg=white;options=bold>',
                    '<fg=white;options=bold>ERROR</fg=white;options=bold>',
                    '<fg=white;options=bold>ID</fg=white;options=bold>'
                )
            )
            ->setRows($lines)
        ;

        $table->render();
    }
}