<?php

namespace Nullsju\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RenderCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('prerender:render')
            ->setDescription('Render url and save static html file')
            ->addOption('url', 'u', InputOption::VALUE_REQUIRED, 'Url to page to be rendered');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $prerender = new \Nullsju\Prerender();
        $render = $prerender->container->get('render');
        $render->run($input->getOption('url'));
    }
}