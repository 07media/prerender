<?php

namespace Nullsju\Exceptions;

/**
 * Define a custom exception class
 */
class RenderErrorException extends \Exception
{
    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 0, Exception $previous = null) {

        parent::__construct($message, $code, $previous);
    }
}
