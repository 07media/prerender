<?php

require __dir__ . "/vendor/autoload.php";

$prerender = new \Nullsju\Prerender();
$queue = $prerender->container->get('queue');

$queue->add("test", "test");
$item = $queue->next();
$queue->done($item['id']);

$queue->add("test2", "test2");
$item = $queue->next();
$queue->error($item['id'], "test error");
